public class NQueens {
    private int[][] map;
    private int size;

    NQueens(int size)
    {
        map = new int[size][size];
        this.size = size;
    }

    void findMap()
    {

        createMap(1);


    }

    private boolean createMap(int nQueenNumber)
    {
        if(nQueenNumber > size)
            return true;
        for (int i = 0; i < size;i++)
        {
            for(int j = 0; j< size;j++)
            {
                if (!isUnderAttack(j,i))
                {
                    map[i][j]=nQueenNumber;
                    boolean isAttach = createMap(nQueenNumber+1);
                    if(isAttach)
                        return true;
                    map[i][j] = 0;
                }
            }
        }
        return false;
    }


    private boolean isUnderAttack(int x,int y)
    {
        for(int underHorizontal = 0;underHorizontal<size;underHorizontal++)
        {
            if(map[y][underHorizontal] != 0)
                return true;
        }

        for(int underVertical = 0;underVertical<size;underVertical++)
        {
            if(map[underVertical][x] != 0)
                return true;
        }


        int[] diagonalPos =diagonalPosition(x,y);
        int diagonalPosX = diagonalPos[0];
        int diagonalPosY = diagonalPos[1];
        while (diagonalPosX<size  &&  diagonalPosY<size)
        {
            if(map[diagonalPosY][diagonalPosX] != 0)
                return true;
            diagonalPosX++;
            diagonalPosY++;
        }

        int[] diagonalPosRev =diagonalPositionReverse(x,y);
        int diagonalPosRevX = diagonalPosRev[0];
        int diagonalPosRevY = diagonalPosRev[1];
        while (diagonalPosRevX >= 0  &&  diagonalPosRevY<size)
        {
            if(map[diagonalPosRevY][diagonalPosRevX] != 0)
                return true;
            diagonalPosRevX--;
            diagonalPosRevY++;
        }

        return false;
    }

    private int[] diagonalPosition(int x,int y)
    {
        while (x >0 && y >0)
        {
            x--;
            y--;
        }
        return new int[]{x,y};
    }

    private int[] diagonalPositionReverse(int x,int y)
    {
        while (x < size-1 && y >0)
        {
            x++;
            y--;
        }
        return new int[]{x,y};
    }


    public  void show()
    {
        for (int i = 0; i < size;i++)
        {
            System.out.print("| ");
            for (int j = 0; j < size; j++)
            {
                System.out.print(map[i][j]+" ");
            }
            System.out.println('|');
        }
    }


    public static void main(String[] args) {
        NQueens nQueens = new NQueens(8);
        nQueens.findMap();
        nQueens.show();
    }
}
